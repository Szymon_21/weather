import sys
from modules.class_Program import Program

def validate(argv):
    if len(argv) < 2:
        raise Exception("Brak kodu API")
    elif len(argv) > 2:
        raise Exception("Za dużo parametrów")

def parser(argv):
    validate(argv)

    if len(argv) == 2:
        return (argv[1])   

api_key = parser(sys.argv)

start = Program(api_key)

start.program_working()