import datetime

def actual_date():
        now_date = datetime.datetime.now()
        current_date = now_date.strptime('%Y-%m-%d')
        return current_date