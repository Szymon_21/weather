import modules.actual_date
import requests
import json

print(22 * '#')
print('PROGRAM POGODOWY po IP')
print('NA NASTĘPNE 3 DNI')
print(22 * '#')
print('\n')


class Program:
    def __init__(self, api_key):
        self.api_key = api_key
        
    def get_data(self):
        api = f'http://api.weatherapi.com/v1/forecast.json?key={self.api_key}&q=auto:ip&days=3&aqi=no&alerts=no&lang=pl'
        self.json_data = requests.get(api).json()

    def read_three_day(self):
        dict_place = 0
        while dict_place != 3:
            location = self.json_data['location']['name']
            date = self.json_data['forecast']['forecastday'][dict_place]['date']
            max_temp = self.json_data['forecast']['forecastday'][dict_place]['day']['maxtemp_c']
            min_temp = self.json_data['forecast']['forecastday'][dict_place]['day']['mintemp_c']
            avg_temp = self.json_data['forecast']['forecastday'][dict_place]['day']['avgtemp_c']
        
            print(f'Temperatura na : {date}:')
            print(f'Lokalizacja to : {location}')
            print(f'Maksymalna temperatura to: {max_temp}')
            print(f'Minimalna temperatura to: {min_temp}')
            print(f'Średnia temperatura to: {avg_temp}')

            dict_place += 1

            print('\n')

        with open('temp_for_three_day', 'w') as f:
            json.dump(self.json_data, f)

    def print_three_day(self):
        try:
            with open('temp_for_three_day', 'r') as f:
                data = json.load(f)

            date_of_file = data['forecast']['forecastday'][1]['date']

            if date_of_file == modules.actual_date:
                 self.read_three_day()

            else:
                dict_place = 0
                while dict_place != 3:
                    location = data['location']['name']
                    date = data['forecast']['forecastday'][dict_place]['date']
                    max_temp = data['forecast']['forecastday'][dict_place]['day']['maxtemp_c']
                    min_temp = data['forecast']['forecastday'][dict_place]['day']['mintemp_c']
                    avg_temp = data['forecast']['forecastday'][dict_place]['day']['avgtemp_c']

                    print('ZAPISANE')
                    print(f'Temperatura na : {date}:')
                    print(f'Lokalizacja to : {location}')
                    print(f'Maksymalna temperatura to: {max_temp}')
                    print(f'Minimalna temperatura to: {min_temp}')
                    print(f'Średnia temperatura to: {avg_temp}')
                    
                    dict_place += 1

                    print('\n')
                    
        except :
            self.read_three_day()

    def program_working(self):
        self.get_data()
        self.print_three_day()